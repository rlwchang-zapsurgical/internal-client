const merge = require('webpack-merge');
const webpack = require('webpack');
const common = require('./webpack.common');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");


module.exports = merge(common, {
  mode: 'development',
  output: {
    filename: "[name].[hash].js",
    // publicPath: "/"

  },
  devServer: {
    compress: true,
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'dist'),
    proxy: {
      "/api": {
        target: "http://127.0.0.1:3000",
        changeOrigin: true,
      }
    },
    hot: true,
    open: true
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true // set to true if you want JS source maps
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css'
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      "process.env": require('./packages/dotenv').webpack.staging
    }),
  ]
})
