
import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash'
import { MessageBar, CircularProgress, LinearProgress } from "ZapWebCommon/lib/js"

export default function requireAuth(roles = [], options = {}) {

  return function (ComposedComponent) {
    let defaultOptions = {
      hide: true,
      error: false
    }

    options = { ...defaultOptions, ...options }

    class Authorization extends Component {
      constructor(props) {
        super(props)

        this.state = {
          isAuthenticated: false,
          isAuthorized: _.isEmpty(roles),
          showError: true
        }
      }

      checkAuthentication(token, currentUser) {
        console.log('Has token:', token)
        console.log('Has id:', currentUser._id)
        console.log('Is Prod:', process.env.NODE_ENV)
        if ((token || currentUser._id) || process.env.NODE_ENV != 'production') {
          this.setState(() => {return { isAuthenticated: true }})
          return true
        } else {
          return false
        }
      }

      checkAuthorization(currentRoles) {
        let authorized = false
        if (_.includes(currentRoles, 'boss') || (_.includes(currentRoles, 'admin') && !options.adminRestricted)) {
          this.setState({ isAuthorized: true })
          return true
        }

        if (typeof roles == 'string')
          roles = [roles]

        roles.forEach(role => {
          if (_.includes(currentRoles, role)) {
            this.setState({ isAuthorized: true })
            authorized = true;
          }})
        
        return authorized
      }
      componentDidMount() {
        const {currentUser} = this.props
        const {token} = currentUser
        if (!currentUser.loading && !this.checkAuthentication(token, currentUser)) { window.location = "/login" }
        this.checkAuthorization(currentUser.roles)
      }

      componentDidUpdate(nextProps, prevState) {
        const { currentUser } = this.props
        const { token } = currentUser
        console.log('Updating...')
        console.log(!_.isEqual(this.props, nextProps) || !_.isEqual(this.state, prevState))
        if (!_.isEqual(this.props, nextProps) || !_.isEqual(this.state, prevState)) {
          // console.log('I got called!')
          // console.log(this.props)
          this.checkAuthentication(this.props.token, this.props.currentUser)
          if (!currentUser.loading && !this.checkAuthentication(token, currentUser)) { window.location = "/login" }
          this.checkAuthorization(this.props.currentUser.roles)
        }
      }

      render() {
        const { token, currentUser={} } = this.props
        const { isAuthenticated, isAuthorized } = this.state
        if (!currentUser._id && token) {
          return (<div>Loading...</div>)
        } else if (!isAuthorized && options.error && this.state.showError) {
          return (<MessageBar
            message='Sorry, you are not authorized to view this section. Please contact an administrator for permission.'
            variant='warning'
            onClose={() => { this.setState({ showError: false }) }}
            className='message-bar warning'
          />)
        } else if (!isAuthorized && options.hide) {
          return null
        } else {
          return (<ComposedComponent {...this.props} isAuthorized={isAuthorized} />)
        }
      }
    }

    function mapStateToProps({ currentUser }) {
      return { currentUser, token: localStorage.getItem('token') };
    }

    return connect(mapStateToProps)(Authorization);
  }
}