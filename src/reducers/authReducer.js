import _ from 'lodash'
import {LOGIN, LOGOUT, RESET_LOGIN, AUTHENTICATE} from "../actions";

export default (state={loading: true}, action) => {
  switch(action.type) {
    case LOGIN:
      return {
        ...action.payload,
        loading: false,
      }
    case LOGOUT:
      return ({
        error: null,
        token: null
      })
    case RESET_LOGIN:
      return _.omit(state, 'mfaRequired')
    case AUTHENTICATE:
      return {
        ...state,
        ...action.payload.data,
        loading: false
      }
    default:
      return state;
  }
}
