import React, { Component } from 'react'

import {connect} from "react-redux";
import Mfa from "./Mfa";

import {login} from "../../actions"


import {Form, Paper, Typography} from 'ZapWebCommon/lib/js';
import "../../styles/main.scss";

const fields = [{
  name: 'username',
  label: 'Username'
}, {
  name: 'password',
  type: 'password',
  label: 'Password'
}, {
  name: 'rememberDevice',
  type: 'checkbox',
  label: 'Remember Device?'
}]

@connect(({currentUser}) => ({
  currentUser
}), {login})
export class Login extends Component {
  state = {
    username: null,
    password: null,
    rememberDevice: localStorage.getItem('rememberDevice') && localStorage.getItem('rememberDevice') != 'false',
    mfaRequired: false
  }  

  handleSubmit = (values) => {
    this.setState(values, () => {
      this.props.login(this.state)
    });
  }

  renderError(error) {
    if(error) {
      return (
        <div className='error'>
          {error}
        </div>
      )
    }
  }
  
  render() {
    const {currentUser} = this.props;

    const params = {
      fields,
      handleSubmit: this.handleSubmit,
      submitButton: 'Login',
      initialValues: {rememberDevice: this.state.rememberDevice},
      enableReinitialize: true
    }

    return (
      <div className='login-form'>
        <Paper className="paper" elevation={1}>
          <Typography variant="headline" gutterBottom>
            Sign into Your Account
          </Typography>
          <Form {...params} />
          {this.renderError(currentUser.error)}
        </Paper>
        <Mfa userData={this.state}/>
      </div>
    )
  }
}

export default Login
