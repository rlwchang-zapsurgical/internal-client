import {combineReducers} from "redux";

import {reducer as formReducer} from "redux-form";
import connectionReducer from "./connectionReducer";
import authReducer from "./authReducer";
import mfa from "./mfa";

const rootReducer = combineReducers({
  form: formReducer,
  connections: connectionReducer,
  currentUser: authReducer,
  mfa
});



export default rootReducer;
