import React from 'react'
import { Card as MaterialCard, Avatar, Icons,
  CardHeader, CardContent, CardActions, Typography, Button
} from 'ZapWebCommon/lib/js';

const Card = (props) => {
  const {title, body, buttonText, icon} = props;
  const Icon = Icons[icon];

  return (
      <MaterialCard className="card">
        <CardHeader
          title={title}
          avatar={
            <Avatar className="avatar">
              <Icon/>
            </Avatar>
          }
        />
        {body &&
          <CardContent>
            <Typography>{body}</Typography>
          </CardContent>
        }
      </MaterialCard>
  )
}

export default Card
