import React, {Component} from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import { connect } from 'react-redux'
import Fingerprint from "fingerprintjs2";


import { Footer, AppHeader } from "../../node_modules/ZapWebCommon/lib/js/"
import Landing from "./Landing"

import Login from "./auth/Login";
import Logout from "./auth/Logout";

import requireAuth from "../hoc/requireAuth";
import { authenticate, logout } from '../actions'

import "../styles/main.scss";


const AppRedirect = (props) => {
  // console.log('props', props)
  const appName = match.params.appname;

  document.location.replace(`${window.location.origin}/apps/${appName}`);
  return null
}


class App extends Component {
  componentDidMount() {
    this.props.authenticate()

    new Fingerprint({
      excludeScreenResolution: true,
      excludeAvailableScreenResolution: true,
    }).get((result, components) => localStorage.setItem('fingerprint', result))
  }

  onClickMenuAccount() {
    
  }

  onClickMenuLogout() {
    this.props.logout()
  }
  
  render() {
    const { currentUser } = this.props;

    const accountLabel = currentUser.username && currentUser.firstName && currentUser.lastName
      ? `${currentUser.firstName} ${currentUser.lastName}`
      : "My account";

    const navProps = {
      logo: {
        url: '/'
      },
      app: {
        url: '/',
        title: 'Enterprise Apps'
      },
      menu: {
        profileImage: currentUser.image,
        icon: 'Person',
        menuItems: [
          {
            icon: 'AccountBox',
            label: accountLabel,
            onClick: this.onClickMenuAccount.bind(this)
          },
          {
            icon: 'ExitToApp',
            label: 'Logout',
            onClick: this.onClickMenuLogout.bind(this)
          }
        ]
      }
    }
    return (
      <BrowserRouter>
        <React.Fragment>
          <AppHeader {...navProps} />
          <div id="content">
            <Switch>
              <Route path="/login" component={Login}/>
              <Route path="/logout" component={requireAuth()(Logout)}/>
              <Route path="/apps/:appname/*" component={requireAuth()(AppRedirect)}/>
              <Route path="/" component={requireAuth()(Landing)} />
            </Switch>
            <Footer/>
          </div>
        </React.Fragment>
      </BrowserRouter>
    )
  }
}

function mapStateToProps({currentUser}) {
  return {
    currentUser
  }
}

export default connect(mapStateToProps, { authenticate, logout })(App);

