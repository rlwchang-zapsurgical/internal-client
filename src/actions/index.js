import axios from "axios";
import Fingerprint from "fingerprintjs2";
import {reduxAjaxRequest} from './helpers'
import {get} from 'lodash-es'

const ENTERPRISE_API_ROOT_URL = process.env.NODE_ENV == 'production' ? "http://zapenterpriseserver-dev.us-west-1.elasticbeanstalk.com/api" : "http://localhost:3000/api";


export const authenticatedRequest = (method, url, data) => {
  return axios({
    method,
    url,
    data,
    headers: {'authorization': localStorage.getItem('token')}
  })
}

export const SET_FINGERPRINT = 'SET_FINGERPRINT';
export function setFingerprint(removeExisting = true) {
  return dispatch => {
    new Fingerprint({
      excludeScreenResolution: true,
      excludeAvailableScreenResolution: true,
    }).get(
      (result, components) => {
        localStorage.setItem('fingerprint', result);
        return dispatch({type: SET_FINGERPRINT, fingerprint: result});
      }
    )
  }
}

export const MFA_PROMPT = 'MFA_PROMPT'
export function mfaPrompt(open=false) {
  return {type: MFA_PROMPT, payload: open}
}

export const LOGIN = 'LOGIN';
export function login(data) {
  return async (dispatch) => {
    const loginError = 'The username or password you entered was incorrect, please try again!'
    const mfaError = 'Incorrect MFA token'
    const {username, password, rememberDevice, mfaToken} = data
    const fingerprint = localStorage.getItem('fingerprint')
    if (fingerprint) {localStorage.setItem('rememberDevice', rememberDevice)}
    try {
      
      const response = await axios.post(
        `${process.env.API_URL}/auth/login`, ({
          username,
          password,
          rememberDevice,
          fingerprint,
          mfaToken
      }))
    
      const token = get(response, 'data.data')
      // console.log('token', token)
    
      if (!token) {
        return dispatch(mfaPrompt(true))
      }
      
      if (token) {
        localStorage.setItem('token', token);
        window.location = "/"
      }
      
      localStorage.setItem('rememberDevice', rememberDevice);
    } catch(err) {
      dispatch({ type: LOGIN, payload: { error: `${ mfaToken ? mfaError : loginError}` } })
      return dispatch(mfaPrompt(false))
    }
  }
}

// export const AUTHENTICATE = 'AUTHENTICATE';
// export function authenticate() {
//   return dispatch => {
//     const request = authenticatedRequest('get', `${ENTERPRISE_API_ROOT_URL}/auth/user`)
//     request.then(payload => dispatch({ type: AUTHENTICATE, payload }))
//       .catch(console.log)
//   }
// }
export const AUTHENTICATE = 'AUTHENTICATE';
export function authenticate() {
  return reduxAjaxRequest({
    actionType: AUTHENTICATE,
    endpoint: 'auth/user',
  })
}

export const LOGOUT = 'LOGOUT';
export function logout() {
  return dispatch => {
    localStorage.removeItem('token');
    const request = axios.post(`${ENTERPRISE_API_ROOT_URL}/auth/logout`);
    return dispatch({type: LOGOUT})
  }
}

export const RESET_LOGIN = 'RESET_LOGIN';
export function resetLogin() {
  return {type: RESET_LOGIN}
}