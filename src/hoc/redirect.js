import React, { Component } from 'react';


export default function(to='/') {
  class Redirect extends Component {

    componentDidMount() {
       window.location = to
    }
      
    render() {
      return null
    }
  }
  return Redirect;
}
