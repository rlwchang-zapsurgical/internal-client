import React, { Component } from 'react'
import createReactClass from 'create-react-class'
import Card from './Card'
import { Paper } from "../../node_modules/ZapWebCommon/lib/js/"
import requireAuth from '../hoc/requireAuth'
class Landing extends Component {
  
  render() {
    const systemMonitoringLink = (process.env.NODE_ENV == 'production') ? '/apps/system-monitoring/' : '../../../Apps/ConfigManagement/dist/'
    const userManagementLink = (process.env.NODE_ENV == 'production') ? '/apps/user-management/' : '../../../Apps/UserManagement/dist/'
    const configManagementLink = (process.env.NODE_ENV == 'production') ? '/apps/config-management/' : '../../../Apps/ConfigManagement/dist/'
    const releaseManagementLink = (process.env.NODE_ENV == 'production') ? '/apps/release-management/' : '../../../Apps/ReleaseManagement/dist/'

    const UserManagement = (props) => {
      return (
            <a href={userManagementLink} className='user-management'>
              <Card
                title="User Management"
                icon="Group"
              />
            </a>
      )
    }
    const AuthUserManagement = requireAuth('admin')(UserManagement)

    return (
      <Paper className='app-list'>
        <div>
          <a href={systemMonitoringLink} className='sys-management'>
            <Card
              title="System Management"
              icon="Computer"
            />
          </a>
          <AuthUserManagement/>
          <a href={configManagementLink} className='sys-config'>
            <Card
              title="Software Configuration Management"
              icon="Settings"
            />
          </a>
          <a href={releaseManagementLink} className='release-config'>
            <Card
              title="Release Management"
              icon="Settings"
            />
          </a>
        </div>
      </Paper>
    )
  }
}

export default Landing;
