const merge = require('webpack-merge');
const webpack = require('webpack');
const path = require('path')
const common = require('./webpack.common');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


module.exports = merge(common, {
  mode: 'development',
  output: {
    filename: "[name].js",
    publicPath: "/"
  },
  devtool: "cheap-module-source-map",
  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'dist'),
    proxy: {
      "/api": {
        target: "http://127.0.0.1:3000",
        changeOrigin: true,
      }
    },
    hot: true,
    open: true
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css'
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      "process.env": require('./packages/dotenv').webpack.dev
    }),
  ],
})
