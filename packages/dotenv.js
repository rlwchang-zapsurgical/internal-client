const dotenv = require('dotenv')
const path = require('path')

const commonVariables = dotenv.config({path: path.resolve(__dirname, '../.env')}).parsed
const devVariables = dotenv.config({path: path.resolve(__dirname, '../.env.dev')}).parsed
const stagingVariables = dotenv.config({path: path.resolve(__dirname, '../.env.staging')}).parsed
const prodVariables = dotenv.config({path: path.resolve(__dirname, '../.env.prod')}).parsed

module.exports = {
  webpack: {
    common: processEnv(commonVariables, {webpack: true}),
    dev: processEnv(devVariables, {webpack: true}),
    staging: processEnv(stagingVariables, {webpack: true}),
    prod: processEnv(prodVariables, {webpack: true})
  }
}


function processEnv(env, options={}) {
  const result = {}
  for (let key in env) {
    const value = env[key]
      if (value.toLowerCase() == 'true') {result[key] = true}
      else if (value.toLowerCase() == 'false') {result[key] = false}
      else {result[key] = value}
  }

  if (options.webpack) {
    for(let key in result) {
      result[key] = `"${result[key]}"`
    }
  }

  return result
}
