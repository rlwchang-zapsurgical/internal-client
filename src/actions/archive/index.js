import axios from "axios";
import {reduxAjaxRequest} from './helpers'

const ENTERPRISE_API_ROOT_URL = process.env.NODE_ENV == 'production' ? "http://zapenterpriseserver-dev.us-west-1.elasticbeanstalk.com/api" : "http://localhost:3000/api";


export const authenticatedRequest = (method, url, data) => {
  return axios({
    method,
    url,
    data,
    headers: {'authorization': localStorage.getItem('token')}
  })
}

export const SET_FINGERPRINT = 'SET_FINGERPRINT';
export function setFingerprint(removeExisting = true) {
  return dispatch => {
    
  }
}

export const MFA_PROMPT = 'MFA_PROMPT'
export function mfaPrompt(open=false) {
  return {type: MFA_PROMPT, payload: open}
}

export const LOGIN = 'LOGIN';
export function login(data) {
  return dispatch => {
    const token = localStorage.getItem('token');
    const fingerprint = localStorage.getItem('fingerprint')
    const loginError = 'The username or password you entered was incorrect, please try again!'
    const request = axios.post(`${ENTERPRISE_API_ROOT_URL}/auth/login`, { ...data, fingerprint});
    console.log(request)

    const { rememberDevice } = data
    localStorage.setItem('rememberDevice', rememberDevice);

    request
      .then(payload => {
        // Store a token to keep session
        const { data: token } = payload.data

        if(error) {
          dispatch({ type: LOGIN, payload: { error: `${error}` } });
        }
        else if (token) {
          // If everything is successful, a token is returned
          localStorage.setItem('token', token);
          // Redirect back to the homepage
          window.location = "/"
          // If mfa is required, return action with mfaRequired
        } else if (mfaRequired) {
          return dispatch({ type: LOGIN, payload: { mfaRequired } })
        }
      })
      // If there is a login error return with an error.
      .catch(error => dispatch({ type: LOGIN, payload: { error: `${loginError}` } }))
  }
}

// export const AUTHENTICATE = 'AUTHENTICATE';
// export function authenticate() {
//   return dispatch => {
//     const request = authenticatedRequest('get', `${ENTERPRISE_API_ROOT_URL}/auth/user`)
//     request.then(payload => dispatch({ type: AUTHENTICATE, payload }))
//       .catch(console.log)
//   }
// }
export const AUTHENTICATE = 'AUTHENTICATE';
export function authenticate() {
  return reduxAjaxRequest({
    actionType: AUTHENTICATE,
    endpoint: 'auth/user',
  })
}

export const LOGOUT = 'LOGOUT';
export function logout() {
  return dispatch => {
    localStorage.removeItem('token');
    const request = axios.post(`${ENTERPRISE_API_ROOT_URL}/auth/logout`);
    return dispatch({type: LOGOUT})
  }
}

export const RESET_LOGIN = 'RESET_LOGIN';
export function resetLogin() {
  return {type: RESET_LOGIN}
}