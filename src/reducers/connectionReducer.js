import {GET_CONNECTIONS} from "../actions";

export default (state={}, action) => {
  switch(action.type) {
    case GET_CONNECTIONS:
      return action.payload.data;
    default:
      return state;
  }
}
