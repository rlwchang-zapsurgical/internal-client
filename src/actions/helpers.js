import axios from 'axios';
import get from 'lodash-es'

export function reduxAjaxRequest(options={}) {
  const {
    actionType = '',
    endpoint = '',
    method = 'get',
    body = {},
    callback=()=>{},
    errback=()=>{}
  } = options

  return (dispatch) => {
    const request = authenticatedAjaxRequest(endpoint, method, body)
    request
      .then(
        data => {
          callback(data)
          return dispatch({type: actionType, payload: data.data})
        })
      .catch(err => {
        console.log(`${method.toUpperCase()} request to ${process.env.API_URL}/${endpoint} failed:`)
        console.log(err)
        errback(err)
    })
  }
}


export function authenticatedAjaxRequest(endpoint='', method='get', body={}) {
  const url = `${process.env.API_URL}/${endpoint}`
  const request = axios({
    method,
    url,
    data: body,
    headers: { authorization: localStorage.getItem("token") }
  });
  return request
}