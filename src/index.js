import React, {Component} from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {applyMiddleware, createStore, compose} from "redux";
// import Promise from "redux-promise";
import Thunk from "redux-thunk";

import rootReducer from "./reducers";
import App from "./components/App";
import {authenticate, AUTHENTICATE} from "./actions"

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  composeEnhancers(applyMiddleware(Thunk))
  // applyMiddleware(Thunk)
);

// store.dispatch({
//   type: LOGIN,
//   payload: {token: localStorage.getItem('token')}
// })

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
, document.querySelector("#root"));
