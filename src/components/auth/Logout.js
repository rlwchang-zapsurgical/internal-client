import React, {Component} from "react";
import {connect} from "react-redux";
import  {logout} from "../../actions"
import { Paper, Typography } from "ZapWebCommon/lib/js/"

class Logout extends Component {

  componentWillMount() {
    this.props.logout();
    setTimeout(() => {this.props.history.push("/")} , 3000)
  }

  render() {
    return (
      <Paper className="logout" elevation={1}>
        <Typography variant="headline" component="h2">
          {this.props.user ? "Logging you out..." : "You have logged out successfully!"}
        </Typography>
        <Typography component="p">
          Redirecting you back to the home page.
        </Typography>
      </Paper>
    )
  }
}

function mapStateToProps({user}) {
  return {user}
}

export default connect(mapStateToProps, {logout})(Logout);
