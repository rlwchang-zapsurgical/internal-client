import _ from 'lodash'
import {MFA_PROMPT} from "../actions";

export default (state=false, action) => {
  switch(action.type) {
    case MFA_PROMPT:
      return action.payload
    default:
      return state;
  }
}
