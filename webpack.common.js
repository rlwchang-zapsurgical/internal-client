const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const VENDOR_LIBS = [
  'react',
  'react-dom',
  'react-router-dom',
  'axios',
  'lodash'
]


module.exports = {
  entry: {
    bundle: "./src/index.js",
    // vendor: VENDOR_LIBS,
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    // publicPath: "/"
  },
  // optimization: {},
  module: {
    rules: [
      {test: /\.jsx?$/, exclude: /node_modules/, loader: "babel-loader"},
      {test: /\.s?css$/, use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        'postcss-loader',
        'sass-loader',
      ]},
      {test: /\.(eot|svg|ttf|woff|woff2|otf)$/, loader: "file-loader", options: {name: "[name].[ext]", outputPath: "fonts/"}},
      {test: /\.(jpg|png|svg)$/, loader: "file-loader"},
      {test: /\.gz$/, enforce: 'pre', use: 'gzip-loader'}
    ]
  },
  plugins: [
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
      showErrors: false,
      chunks: ["vendor", "bundle", "manifest"]
    }),
    new CopyWebpackPlugin([
      { from: "./public/icons", to: "./icons"}
    ]),
    // new BundleAnalyzerPlugin()
  ]
}
