import React, { Component } from 'react'
import { ModalDialog, Form } from 'ZapWebCommon/lib/js'
import { connect } from 'react-redux'

import {login, resetLogin, mfaPrompt} from '../../actions'

const fields = [{
    name: 'mfaToken',
    placeholder: '000000'
}]

@connect(({currentUser, mfa}) => ({
    currentUser,
    mfa
}), {login, resetLogin, mfaPrompt})
export class Mfa extends Component {

    params = {
        fields,
        disableOnSubmit: true,
        handleSubmit: (values) => this.handleSubmit(values),
        submitButton: 'Verify'
    }

    handleClose = () => {
        this.props.resetLogin()
        this.props.mfaPrompt(false)
    }

    handleSubmit(values) {
        const {login, userData} = this.props;
        login({...userData, ...values})
    }

    render() {
        const {currentUser} = this.props;

        return (
            <ModalDialog
                open={this.props.mfa}
                onClose={this.handleClose}
                title={'A vertification code has been sent to your phone. Please enter the code below.'}
            >
                <div className='mfa-form'>
                    <Form {...this.params} />
                </div>
            </ModalDialog>
        )
    }
}

export default Mfa